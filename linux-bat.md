
###### 安装依赖
```
npm install
```
######  建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
```
npm install --registry=https://registry.npm.taobao.org
```
######  启动服务

```
npm run dev
```
######  构建生产环境
```
npm run build:prod
```

####  其它

###### 代码格式检查
```
npm run lint
```
######  代码格式检查并自动修复
```
npm run lint -- --fix
```


------------------------------创建Vue工程---------------------------

创建vue工程项目
######  UI界面创建工程
```
vue ui
```
######  安装ui界面
```
npm install -g @vue/cli
```
######  npm命令创建文件夹
```
vue init webpack filename
```

    ? Project  vue-demo # 项目名称，直接回车，按照括号中默认名字（注意

    这里的名字不能有大写字母，如果有会报错Sorry,  can no longer contain capital letters）
    ? Project description A Vue.js project # 项目描述,随便写
    ? Author # 作者名称
    ? Vue build standalone # 我选择的运行加编译时
    	Runtime + Compiler: recommended for most users
    ? Install vue-router? Yes # 是否需要 vue-router，路由肯定要的
    ? Use ESLint to lint your code? Yes # 是否使用 ESLint 作为代码规范.
    ? Pick an ESLint preset Standard # 一样的ESlint 相关
    ? Set up unit tests Yes # 是否安装单元测试
    ? Pick a test runner 按需选择 # 测试模块
    ? Setup e2e tests with Nightwatch? 安装选择 # e2e 测试
    ? Should we run `npm install` for you after the project has been created? (recommended) npm # 包管理器，选的NPM--
---------------------------------------npm设置------------------------------------
######  修改下载仓库为淘宝镜像
```
npm config set registry http://registry.npm.taobao.org/
```
###### 如果要发布自己的镜像需要修改回来
```
npm config set registry https://registry.npmjs.org/
```
---------------------------------------运行Vue工程-------------------------------
######  安装依赖
```
npm install
```
######  运行
```
npm run dev
```
--------------------------------安装cli3卸载cli3---------------------------
######  卸载cli3
```
npm uninstall -g @vue/cli
```
######  安装cli3
```
npm install -g @vue/cli
```
------------------------------------生成安卓密钥-------------------------------
######  使用keytool -genkey命令生成证书：
```
keytool -genkey -alias testalias -keyalg RSA -keysize 2048 -validity 36500 -keystore filname.keystore
```
######  查看证书信息
```
keytool -list -v -keystore filname.keystore
```
-------------------------创建Node.js工程-----------------------------------------
######  启动程序
```
supervisor index
```
######  全局安装Express
```
npm install express -g
```
--------------------linux安装Cockpit控制台----------------------
######  安装 cockpit:ip:9090端口
sudo dnf install cockpit
######  打开 cockpit:
sudo systemctl enable --now cockpit.socket

--------------------linux打开docker运行镜像----------------------
######  docker拉去epic自动镜像
```
docker pull luminoleon/epicgames-claimer
```
######  docker打开自动 每次需要登录
```
docker run -it luminoleon/epicgames-claimer
```


######  docker打开自动 一次登录
```
docker run -it -v ~/epicgames_claimer/User_Data:/User_Data luminoleon/epicgames-claimer
```
######  云服务器创建窗口
```
screen -S docker
```
######  云服务器恢复窗口
```
screen -r docker
```

------------------ubuntu安装nginx------------------

######  切换至root用户
```
sudo su root
```
```
apt-get install nginx
```
######  查看nginx是否安装成功
```
nginx -v
```
-----------------------ubuntu 修改root账户登录--------------------
######  切换至root用户
```
sudo su root
```
######  打开配置文件
```
sudo vi /etc/ssh/sshd_config
```
PermitRootLogin yes
esc :wq
######  重启 ssh 服务。
```
sudo service ssh restart
```
-------------------------关于nginx-------------------
######  执行以下安装命令
```
sudo apt-get install nginx
```
######  安装完成，查看版本来检测是否安装成功。
```
sudo apt-get install nginx
```

nginx文件安装完成之后的文件位置：
    /usr/sbin/nginx：主程序
    /etc/nginx：存放配置文件
    /usr/share/nginx：存放静态文件
    /var/log/nginx：存放日志
 
######  修改配置后重新加载生效  
```
nginx -s reload
```
######  重新打开日志文件
```
nginx -s reopen
```
######  测试nginx配置文件是否正确
```
nginx -t -c /path/to/nginx.conf
```
关闭nginx
    快速停止nginx
```
nginx -s stop
```
    完整有序的停止nginx
```
nginx -s quit
```

其他的停止nginx 方式：
ps -ef | grep nginx
kill -QUIT 主进程号     ：从容停止Nginx
kill -TERM 主进程号     ：快速停止Nginx
pkill -9 nginx          ：强制停止Nginx
启动nginx:
nginx -c /etc/nginx/nginx.conf
平滑重启nginx：
kill -HUP 主进程号

######  启动nginx
```
service nginx start
```
######  关闭nginx：
```
nginx -s stop
```
-------------------------------linux命令------------------------------

######  删除回收站
```
sudo rm -rf ~/.local/share/Trash/*
```
######  安装screen
```
sudo apt install screen
```
######  新建名字为qszl的窗口
```
screen -S qszl
```
######  分离会话
快键键Ctrl+a+d实现分离 或者
```
screen -ls
```
######  恢复名字为qszl的窗口
```
screen -r qszl 或 screen -r 27582
```
######  杀死会话窗口
```
kill -4406 threadnum
```
######  清除死去窗口
```
screen -wipe
```

---------------------------------------CMD--Bat命令-------------------------------
######  创建日期文件夹bat
```
set "ymd=%date:~0,4%%date:~5,2%%date:~8,2%"
set "hms=%time:~0,2%%time:~3,2%%date:~8,2%"
md D:\temp\class%ymd%_%hms%
```
###### 管理员安装msi 
```
msiexec /package "mysql.msi"
```
######  host文件夹位置
```
C:\Windows\System32\drivers\etc
```




